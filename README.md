# Modeling of the Silicon Carbide Silicon Dioxide Interface using Density Functional Theory
Written by I.Bouquet from Integreted Systems Laboratory at ETH Zurich.

A three-steps method is implemented for the investigation of the interfacial defects.
1. Interface creation (SiC bulk + layer-by-layer deposition of the SiO2 using ReaxFF)
2. Ground state geometry optimization within DFT
3. Computing of the wavefunctions of the defect states 

The LAMMPS package is used to perform classical MD and geometry optimization with reactive force fields. All DFT simulations are executed using the CP2K tool. To describe the exchange-correlation potential the PBE functional is used in most simulations. Moreover, the double zeta-valence polarized (DZVP) basis set and GTH-pseudo potentials are employed. In order to visualize the interfaces the VMD software can used (given a .xyz file as input).

## Interface creation (Layer-by-layer deposition)

Layers of Si and O are deposited on the SiC carbide surface alternatively, allowing the continuous optimization of the growing SiO2 with reactive force-fields.

Inputs:

SiC_bulk.pos: SiC bulk, in a format lammps can read

control: a file needed by lammps

minimize-interface.in / minimize.in: input file that controls the simulation (FF, boundary conditions, temperature, etc.)

ffield_CHOSiC_Newsome / ffield_CHOWaterAlSiFe: contain the FF parameters for the SiC-SiO2 interface and SiO2 reaxFFs

functions.py: some python functions needed by the run_script_lammps.py

lmp_omp-alvier: lammps executable (will only run on a host server, not on your computer)

run_scripts_lammps.py: used to run the simulation (SiC_bulk.pos, SiO2 length (nm), nb layers, minimal distance between deposited atoms (nm), host, nb of interfaces)

`./run_scripts_lammps.py SiC_bulk.pos 40 24 2 host_name 10`


Output example: SiC_lbl_10h22.xyz

## Geometrical optimization DFT 

The created interfaces undergo a ground state geometry optimization using DFT.

geo_opt.inp: Input script for geometry optimization (check lattice parameters + path of the potential and basis set file)

SiC_lbl_10h22.xyz: Interface obtained from the layer by layer deposition

`mpirun -n 2 cp2k.popt -o geo_opt.out -i geo_opt.inp`

Main outputs:

1. geo-opt-pos-1.xyz: Recorded atoms positions for each itteration
2. geo-opt-RESTART.wfn: Final Kohn-Sham wavefunctions from the calculations


## Defects investigation

From geo-opt-RESTART.wfn we can compute the spatial distribution of the electron density (WFN) in real space of the defect state.

defect_states.inp: Input script for printing the .cube files describing the electronic densities

geo-opt-RESTART.wfn:electronic wavefunctions (from geometry optimization in DFT)

SiC_lbl_10h22_opt.xyz: optimized interface

`mpirun -n 2 cp2k.popt -o defect_states.out -i defect_states.inp`

Outputs are a serie of .cube files (as many as stated in the &ENERGY WINDOW section) that can be visualized using the VMD software.
