@SET si_potential GTH-PBE-q4
@SET o_potential GTH-PBE-q6
@SET cu_potential GTH-PBE-q11
@SET si_basis_set DZVP-MOLOPT-GTH
@SET o_basis_set DZVP-MOLOPT-GTH
@SET cu_basis_set DZVP-MOLOPT-SR-GTH
@SET print_step 1
@SET daint 0

&GLOBAL
  PROJECT geo-opt
  RUN_TYPE GEO_OPT
  PRINT_LEVEL MEDIUM
  EXTENDED_FFT_LENGTHS
@IF ${daint}
# limit the run time [s]
  WALLTIME 7000
  &FM
    FORCE_BLOCK_SIZE
    TYPE_OF_MATRIX_MULTIPLICATION DBCSR_MM
  &END FM
@ENDIF
&END GLOBAL

&FORCE_EVAL
  METHOD Quickstep
  STRESS_TENSOR ANALYTICAL
  &SUBSYS
    &CELL
      A 9.22799 0 0
      B 0 10.77 0
      C 0 0 50
      SYMMETRY ORTHORHOMBIC
    &END CELL
    &KIND O
      BASIS_SET ${o_basis_set}
      ELEMENT O
      POTENTIAL ${o_potential}
    &END KIND
    &KIND C
      BASIS_SET ${si_basis_set}
      ELEMENT C
      POTENTIAL ${si_potential}
    &END KIND
    &KIND H
      BASIS_SET ${si_basis_set}
      ELEMENT H
      POTENTIAL GTH-PBE
    &END KIND
    &KIND Si
      BASIS_SET ${si_basis_set}
      ELEMENT Si
      POTENTIAL ${si_potential}
    &END KIND
    &TOPOLOGY
      COORD_FILE_NAME SiC_lbl_10h22.xyz
      COORD_FILE_FORMAT XYZ
    &END TOPOLOGY
  &END SUBSYS
  &DFT
@IF ${daint}
    POTENTIAL_FILE_NAME /users/ducryf/scripts/inp-template/POTENTIALS
    BASIS_SET_FILE_NAME /users/ducryf/scripts/inp-template/BASIS_ZIJLSTRA
    BASIS_SET_FILE_NAME /users/ducryf/scripts/inp-template/BASIS_MOLOPT
@ENDIF
    BASIS_SET_FILE_NAME /home/ducryf/scripts/inp-template/BASIS_ZIJLSTRA
    BASIS_SET_FILE_NAME /home/ducryf/scripts/inp-template/BASIS_MOLOPT
    &QS
      METHOD GPW
      EPS_DEFAULT 1.0E-12
      EXTRAPOLATION PS
    &END QS
    &MGRID
      NGRIDS 5
      CUTOFF 500
      REL_CUTOFF 60
    &END MGRID
    &XC
      &XC_FUNCTIONAL PBE
      &END XC_FUNCTIONAL
    &END XC
#    WFN_RESTART_FILE_NAME negf-step-1456-RESTART.wfn
    &SCF
      &PRINT
        &RESTART_HISTORY OFF
        &END RESTART_HISTORY
        &RESTART
          BACKUP_COPIES 0
        &END RESTART
      &END PRINT
      SCF_GUESS RESTART
      EPS_SCF 1.0E-6
      MAX_SCF 30
      CHOLESKY INVERSE_DBCSR
      &OT
        PRECONDITIONER FULL_SINGLE_INVERSE
        ENERGY_GAP 1
        ALGORITHM IRAC
        MINIMIZER DIIS
      &END OT
      &OUTER_SCF
         EPS_SCF 1.0E-6
         MAX_SCF 10
      &END OUTER_SCF
    &END SCF
    &PRINT
      &HIRSHFELD OFF
        FILENAME chg
        SHAPE_FUNCTION DENSITY
      &END HIRSHFELD
      &MULLIKEN OFF
        FILENAME chg
      &END MULLIKEN
    &END PRINT
  &END DFT
&END FORCE_EVAL

&MOTION
  &GEO_OPT
    OPTIMIZER BFGS
    MAX_ITER 100
    &BFGS
    &END BFGS
  &END GEO_OPT
!  &CONSTRAINT
!    &FIXED_ATOMS
!      LIST 97..192
!    &END FIXED_ATOMS
!  &END CONSTRAINT
  &PRINT
    &TRAJECTORY
      &EACH
        CELL_OPT ${print_step}
        GEO_OPT ${print_step}
        MD ${print_step}
      &END EACH
    &END TRAJECTORY
    &CELL
      &EACH
        CELL_OPT ${print_step}
        GEO_OPT ${print_step}
        MD ${print_step}
      &END EACH
    &END CELL
    &FORCES
      &EACH
        CELL_OPT ${print_step}
        GEO_OPT ${print_step}
        MD ${print_step}
      &END EACH
    &END FORCES
      &STRESS
        FILENAME 
        &EACH
          CELL_OPT ${print_step}
          GEO_OPT ${print_step}
          MD ${print_step}
        &END EACH
      &END STRESS
    &RESTART_HISTORY OFF
    &END RESTART_HISTORY
    &RESTART
      BACKUP_COPIES 0
      &EACH
        CELL_OPT ${print_step}
        GEO_OPT ${print_step}
        MD ${print_step}
      &END EACH
    &END RESTART
  &END PRINT
&END MOTION

