import numpy as np
from operator import itemgetter

def read_lammps_pos(fname):
	with open(fname, 'r') as file:
		file.readline()                          	#Discard the comment line
		file.readline()
		no_atom=int(file.readline().split()[0])         # Read the first line
		no_type=int(file.readline().split()[0])
		file.readline()
		latt=np.zeros(3)
		mass=np.zeros(no_type)
		coord=np.zeros((no_atom,4))
		# Cell dimensions
		for i in range(0,3):
			latt[i]=float(file.readline().split()[1])
		# discard
		for i in range(0,3):
			file.readline()
		# mass
		for i in range(0,no_type):
			mass[i]=float(file.readline().split()[1])
		# discard
		for i in range(0,3):
			file.readline()
		for i in range(0,no_atom):
			coord[i,:]=itemgetter(1,3,4,5)(file.readline().split())
	np.sort(coord,axis=-1)
	# positions
	#coord = np.genfromtxt(fname,skip_header=13+mass.size)[:,(1,3,4,5)]
	return coord, mass, latt

def write_lammps_pos(fname, coord, mass, latt):
	with open(fname,'w+') as file:
		file.write('#This is a useless header\n')
		# # of atoms and # of types
		file.write("  %d atoms\n  %d atom types\n\n" % (coord.shape[0],mass.size))
		# cell dimensions
		file.write(" 0.0   %f  xlo xhi\n 0.0   %f  ylo yhi\n 0.0   %f  zlo zhi\n\n" % (latt[0],latt[1],latt[2]))
		# masses
		file.write("Masses\n\n")
		for i in range(0,mass.size):
			file.write(" %d %f\n" % (i+1,mass[i]))
		# atoms
		file.write("\nAtoms\n\n")
		for i in range(0,coord.shape[0]):
			file.write("%d %d 0   %f   %f   %f\n" % (i+1,coord[i,0],coord[i,1],coord[i,2],coord[i,3]))
	return
		

def read_cp2k_data(filename):
    with open(filename) as fid:
        fid.readline() # header
        data = []
        for line in fid:
            data.append(list(map(float,line.split())))
    return data

def read_xyz(filename):
    with open(filename) as fid:
        natoms = int(float(fid.readline()))
        v = fid.readline()[:-1]
        if re.search('(?<=Lattice=")[ 0-9\.\-eE\+]*',v):
          #print([float(a) for a in re.findall('(?<=Lattice=")[ 0-9\.\-eE\+]*',v)[0].split()])
          vec=np.reshape([float(a) for a in re.findall('(?<=Lattice=")[ 0-9\.\-eE\+]*',v)[0].split()],(3,3),order='C')
          #print(vec)
        else:
          vec=np.diag([float(a) for a in v.split()[-3:]])
        vec = np.array(vec)
        species = []
        coords = []
        for ii in range(0, natoms):
            l = fid.readline()[:-1]
            coords.append([float(str.split(l)[1]), float(str.split(l)[2]), float(str.split(l)[3])])
            species.append(str.split(l)[0])
    return np.array(coords), np.array(species), vec

def write_xyz(file_name,coords,species,vec):
    if vec.size==3:
      vec=[vec[0],0,0,0,vec[1],0,0,0,vec[2]]
    else:
      vec=vec.reshape((9),order='C').tolist()
    with open(file_name,'w') as fid:
        fid.write(str(int(np.shape(coords)[0])) + '\n')
        fid.write("Lattice=\"" + ' '.join(map(str,vec)) + '" ! cell of ' + file_name + '\n')
        for ii in range(0, np.shape(coords)[0]):
            fid.write(species[ii] + '  ' + '  '.join(map(str,coords[ii,:])) + '\n')




#if __name__ == '__main__':
#	c,m,v=read_lammps_pos('Lattice.pos')
#	write_lammps_pos('test.pos',c,m,v)
