#! /usr/bin/env python
import os, sys, math, re, time
import numpy as np
from shutil import copy
from functions import read_lammps_pos, write_lammps_pos, write_xyz

# -------------------------------------------------------------
# Some constants:
# -------------------------------------------------------------
rho_SiO2=2.2e-27 # [g/A^3]
u=1.66e-27 # [kg]
mO=15.999 # [u]
mSi=28.085 # [u]
mSiO2=60.083 #[u]


def repetition_LbL (fname_bulk,SiO2_length,nb_layers,Rmin,host,repetition):
	for i in range(repetition):
		print("Starting repetition " + str(i))
		layered_SiO2(fname_bulk,SiO2_length,nb_layers,Rmin,host)
		print("repetition "+str(i)+" ended")
	return

def layered_SiO2 (fname_bulk,SiO2_length,nb_layers,Rmin,host):
	lmp='mpiexec -n 1 -host '+host+' lmp_omp-alvier -sf omp -pk omp 8 '
	copy(fname_bulk,'Lattice.pos')
	for i in range(0,4):
		layer_by_layer('Lattice.pos',SiO2_length,nb_layers,i,Rmin)
		os.system(lmp+' -in minimize-interface.in')
		copy('minimize.pos','Lattice.pos')
	for i in range(4,nb_layers):
		layer_by_layer('Lattice.pos',SiO2_length,nb_layers,i,Rmin)
		os.system(lmp+' -in minimize.in')
		copy('minimize.pos','Lattice.pos')
	c,m,v=read_lammps_pos('Lattice.pos')
	t=['C ','Si','O ']
	s=[]
	for i in range(0,c.shape[0]):
		s.append(t[int(c[i,0])-1])
	c=c[:,1:]
	temps=time.strftime("%Hh%M",time.gmtime())
	write_xyz('SiC_lbl_'+temps+'.xyz',c,s,v)
	os.remove('Lattice.pos')
	return

def layer_by_layer(fname,SiO2_length,nb_layers,layer_ind,Rmin):
    c,mass,v=read_lammps_pos(fname)
    a=v[0]
    b=v[1]

# Determination of the number of atoms per added layer
    w_layer=SiO2_length/nb_layers  
    #c=c+w_layer                     #new lattice parameter
    v[2]+=w_layer
    V_SiO2=a*b*SiO2_length                    #Volume in A^3
    mtot_SiO2= rho_SiO2*V_SiO2/u             #mass of SiO2 in u
    nb_SiO2=round(mtot_SiO2/mSiO2)
    nbtot_Si=nb_SiO2
    nbtot_O=nb_SiO2*2

    nb_Si_layer=int(math.floor(nbtot_Si/(nb_layers/2)))
    nb_O_layer=2*nb_Si_layer
 
    if (layer_ind % 2) == 0:
        nb_atoms=nb_O_layer
        element='O'
        at_type=3
    else:
        nb_atoms=nb_Si_layer
        element='Si'
        Rmin=Rmin*1.5
        at_type=2

# Creating random positions in direction A and B
    AB=np.zeros((nb_atoms,2),dtype=float)
    AB[0,:]=np.random.random((1,2))*[a,b]
    Rmin2=Rmin*Rmin
    for i in range(1,nb_atoms):
        t=np.random.random((1,2))*[a,b]
        dist2=np.square(AB-t).sum(axis=1)
        itteration=0
        while all(dist2>Rmin2)==False:
            t=np.random.random((1,2))*[a,b]
            dist2=np.square(AB-t).sum(axis=1)
            itteration=itteration+1
            if itteration>4000:
                print('max itteration')
                break

        #print(itteration)
        AB[i,:]=t

    A=[0]*nb_atoms
    B=[0]*nb_atoms
    C=[0]*nb_atoms
    c_new = np.vstack((c,np.hstack((np.ones((nb_atoms,1))*at_type,AB,np.ones((nb_atoms,1))*(c[-1,-1]+0.5)))))
    write_lammps_pos('Lattice.pos',c_new,mass,v)
    return

if __name__ == '__main__':
        start_time = float(time.time())
        fname_bulk = sys.argv[1]
        SiO2_length = float(sys.argv[2])
        nb_layers =int(sys.argv[3])
        Rmin=float(sys.argv[4])              #in Angstrom
        #HT=int(sys.argv[5])
        host=sys.argv[5]
	repetition=int(sys.argv[6])
        repetition_LbL(fname_bulk,SiO2_length,nb_layers,Rmin,host,repetition)
        print "Walltime: %d [s]\n" % (float(time.time())-start_time)

